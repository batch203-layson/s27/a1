// Activity s27






// 1. Create sample documents following the models we have created for users and courses for our booking-system.

	// - user1 and user2 are the ids for the user documents.
	// - course1 and course2 are the ids for the course documents.

	// - user1 is enrolled in course1.
	// - user2 is enrolled in course2.

user {

	"id": "user1",
	"username": "user01",
	"firstName": "fname01",
	"lastName": "lname01",
	"email": "email01",
	"password": "pword01",
	"mobileNumber": "123456789",
	"isAdmin": false,
}




user {

	"id": "user2",
	"username": "user02",
	"firstName": "fname02",
	"lastName": "lname02",
	"email": "email02",
	"password": "pword02",
	"mobileNumber": "987654321",
	"isAdmin": false,
	

}


course {

	"id": "course1",
	"name": "course01",
	"description": "description01",
	"price": "price01",
	"slots": 2,
	"schedule": "Monday - Friday, 8:00 - 9:00",
	"instructor": "instructor01",
	"isActive": true,
	

}



course {

	"id": "course2",
	"name": "course02",
	"description": "description02",
	"price": "price02",
	"slots": 2,
	"schedule": "Monday - Friday, 9:00 - 10:00",
	"instructor": "instructor02",
	"isActive": true,
	

}


// 2 Model Booking System with Embedding

	// - user1 is enrolled in course1.
	// - user2 is enrolled in course2.

user {

	"id": "user1",
	"username": "user01",
	"firstName": "fname01",
	"lastName": "lname01",
	"email": "email01",
	"password": "pword01",
	"mobileNumber": "123456789",
	"isAdmin": false,
	"enrollments": [
		{

			"id": "course1",
			"courseId": "course1",
			"courseName": "course01",
			"isPaid": true,
			"dateEnrolled": "August 01, 2022"
		}
	]

}




user {

	"id": "user2",
	"username": "user02",
	"firstName": "fname02",
	"lastName": "lname02",
	"email": "email02",
	"password": "pword02",
	"mobileNumber": "987654321",
	"isAdmin": false,
	"enrollments": [
		{

			"id": "course2",
			"courseId": "course2",
			"courseName": "course02",
			"isPaid": true,
			"dateEnrolled": "August 02, 2022"
		}
	]

}


course {

	"id": "course1",
	"name": "course01",
	"description": "description01",
	"price": "price01",
	"slots": 2,
	"schedule": "Monday - Friday, 8:00 - 9:00",
	"instructor": "instructor01",
	"isActive": true,
	"enrollees": [

		{
			"id": "user1",
			"userId": "user1",
			"userName": "user01",
			"isPaid": true,
			"dateEnrolled": "August 01, 2022"
		}

	]

}



course {

	"id": "course2",
	"name": "course02",
	"description": "description02",
	"price": "price02",
	"slots": 2,
	"schedule": "Monday - Friday, 9:00 - 10:00",
	"instructor": "instructor02",
	"isActive": true,
	"enrollees": [

		{
			"id": "user2",
			"userId": "user2",
			"userName": "user02",
			"isPaid": true,
			"dateEnrolled": "August 02, 2022"
		}

	]

}



//3 Model Booking System with Referencing



user {

	"id": "user1",
	"username": "user01",
	"firstName": "fname01",
	"lastName": "lname01",
	"email": "email01",
	"password": "pword01",
	"mobileNumber": "123456789",
	"isAdmin": false,
}




user {

	"id": "user2",
	"username": "user02",
	"firstName": "fname02",
	"lastName": "lname02",
	"email": "email02",
	"password": "pword02",
	"mobileNumber": "987654321",
	"isAdmin": false,
}



course {

	"id": "course1",
	"name": "course01",
	"description": "description01",
	"price": "price01",
	"slots": 2,
	"schedule": "Monday - Friday, 8:00 - 9:00",
	"instructor": "instructor01",
	"isActive": true,
	"user_id": "user1"
}



course {

	"id": "course2",
	"name": "course02",
	"description": "description02",
	"price": "price02",
	"slots": 2,
	"schedule": "Monday - Friday, 9:00 - 10:00",
	"instructor": "instructor02",
	"isActive": true,
	"user_id": "user2"
}



enrollment: 
{
	"id": "user1",
	"userId": "user1",
	"userName": "user01",
	"courseId": "course1",
	"courseName": "course01",
	"isPaid": true,
	"dateEnrolled": "August 01, 2022"
}

enrollment: 
{
	"id": "user2",
	"userId": "user2",
	"userName": "user02",
	"courseId": "course2",
	"courseName": "course02",
	"isPaid": true,
	"dateEnrolled": "August 02, 2022"
}



// 3. Create a new repo called s27

//4. Initialize your local repo, add and commit with the following message: "Add Activity Code"

// 5. Push and link to boodle (WDC028v1.5b-27 | MongoDB - Data Modeling and Translation).